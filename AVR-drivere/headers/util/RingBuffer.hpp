#include "config.h"

#ifndef DRIVERS_RINGBUFFER_HPP_
#define DRIVERS_RINGBUFFER_HPP_

namespace MISC {

class RingBuffer
{
public:
	RingBuffer(byte * buffer, uint8_t size);
	int16_t Read(void);
	void Write(byte c);
	uint8_t Count();
	uint8_t Capacity();
	uint8_t read;
	uint8_t write;
	uint8_t size;
private:
	byte * buffer;

};

} /* namespace MISC */

#endif /* DRIVERS_RINGBUFFER_HPP_ */
