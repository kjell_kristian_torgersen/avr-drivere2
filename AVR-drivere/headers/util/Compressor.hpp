#ifndef COMPRESSOR_HPP_
#define COMPRESSOR_HPP_
#include "config.h"
#include <stdint.h>
namespace CMP {

class Compressor {
public:
	Compressor();
	void Write(uint16_t current);
	static void (*ByteWrite)(byte byte);
private:
	static void WriteNibble(byte nibble);
	void WriteValue(uint16_t current);
	void Repeat(void);
	uint16_t previous;
	uint8_t counter;
	static byte _byte;
	static bool high;
};

} /* namespace CMP */

#endif /* COMPRESSOR_HPP_ */

