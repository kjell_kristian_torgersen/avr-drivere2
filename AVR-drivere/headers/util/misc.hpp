#include "config.h"

#ifndef MISC_HPP_
#define MISC_HPP_



namespace MISC {

int16_t xprintf_p(bput_ptr bput, const char * format, ...);
void applybn(bput_ptr bput, const char * str, const uint16_t n);
void xputs(bput_ptr bput, const char * str);
void xputs_p(bput_ptr bput, const char * str);
void TransmitDataPackage(bput_ptr bput, const byte * payload,
		const uint8_t length);
void * sbrk(uint8_t size);

void PackageInit(byte* packagebuffer, uint8_t buffersize, void (*PackageReceivedCallback)(byte * package));
void ReceivePackage(byte b);

}

#endif /* MISC_HPP_ */
