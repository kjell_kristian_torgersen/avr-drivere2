#ifndef CONFIG_H_

#include <stdint.h>

#define CONFIG_H_

#define PCF8574_ADDRESS 0x3F
#define PACKAGE_START_BYTE 0xAA
#define UART_RXBUF_SIZE 16

#ifdef PC
#define PSTR(x) x
#else
#include <avr/pgmspace.h>
#endif
typedef uint8_t byte;
typedef uint8_t (*bput_ptr)(byte);

#endif /* CONFIG_H_ */
