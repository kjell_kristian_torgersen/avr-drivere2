#ifndef HEADERS_HAL_PCF8574_HPP_
#define HEADERS_HAL_PCF8574_HPP_

namespace PCF8574 {

void Init(uint8_t prescaler);
uint8_t SetOutput(byte address, byte data);

}

#endif /* HEADERS_HAL_PCF8574_HPP_ */
