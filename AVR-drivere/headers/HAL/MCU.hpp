#include "config.h"
#include "util/RingBuffer.hpp"

#ifndef MCU_HPP_
#define MCU_HPP_

#define UART_PRESCALER(baud) (((2 * F_CPU / (baud * 16UL))) - 1)

enum {
	BIT0 = 1,
	BIT1 = 2,
	BIT2 = 4,
	BIT3 = 8,
	BIT4 = 16,
	BIT5 = 32,
	BIT6 = 64,
	BIT7 = 128
};

namespace MCU {

extern uint16_t TIMER_ticks(void);
extern uint16_t TIMER_s(void);
extern void (*TIMER_tick)(void);
extern void (*TIMER_sec)(void);
void TIMER_Init(void);

void UART_Init(uint16_t prescaler, MISC::RingBuffer * uarttxbuf, MISC::RingBuffer * uartrxbuf);
byte UART_putchar(byte c);
int16_t UART_getchar(void);

void SPI_Init();
byte SPI_putchar(byte c);

void ADC_Init(void(*adc_callback)(uint16_t newSample), char ref, char channel);
void ADC_Stop();

void I2C_Init(uint8_t prescaler);
int I2C_Start(unsigned char address);
void I2C_Stop(void);
byte I2C_Read(byte ack);
byte I2C_Write(byte data);
#define I2C_READ    1
#define I2C_WRITE   0

int16_t SWUART_putchar(uint16_t SPEED, volatile uint8_t & PORT, uint8_t pin, byte byte);
void Delay_us(uint16_t cycles);
void Delay_ms(uint16_t ms);
void EnableInterrupts(void);
void DisableInterrupts(void);
void BitSet(uint8_t port, uint8_t mask);
void BitClear(uint8_t port, uint8_t mask);
}

#endif /* MCU_HPP_ */
