#ifndef HEADERS_W25QB64_HPP_
#define HEADERS_W25QB64_HPP_

namespace W25Q64 {
void Write(const uint16_t page, const uint8_t offset, const byte * data, const uint16_t n);
void Read(const uint16_t page, const uint8_t offset, byte * data, const uint16_t n);
void ChipErase(void);
byte Status(void);
}

#endif /* HEADERS_W25QB64_HPP_ */
