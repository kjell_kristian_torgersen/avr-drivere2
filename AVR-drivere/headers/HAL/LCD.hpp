#ifndef HEADERS_LCD_HPP_
#define HEADERS_LCD_HPP_
#include "config.h"

/* display on/off, cursor on/off, blinking char at cursor position */
#define LCD_DISP_OFF             0x08   /* display off                            */
#define LCD_DISP_ON              0x0C   /* display on, cursor off                 */
#define LCD_DISP_ON_BLINK        0x0D   /* display on, cursor off, blink char     */
#define LCD_DISP_ON_CURSOR       0x0E   /* display on, cursor on                  */
#define LCD_DISP_ON_CURSOR_BLINK 0x0F   /* display on, cursor on, blink char      */

namespace LCD {
uint8_t Init(byte dispAttr, byte address);
byte putchar(byte c);
void GotoXY(uint8_t x, uint8_t y);
void Clear(void);
void LED(byte onoff);
}

#endif /* HEADERS_LCD_HPP_ */
