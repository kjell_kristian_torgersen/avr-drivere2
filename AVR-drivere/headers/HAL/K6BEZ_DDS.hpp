/*
 * K6BEZ_DDS.hpp
 *
 *  Created on: Feb 15, 2018
 *      Author: kjell
 */

#ifndef HEADERS_HAL_K6BEZ_DDS_HPP_
#define HEADERS_HAL_K6BEZ_DDS_HPP_

namespace K6BEZ_DDS {
//void send_byte(byte data_to_send);
void SetFrequency(uint32_t f);
}

#endif /* HEADERS_HAL_K6BEZ_DDS_HPP_ */
