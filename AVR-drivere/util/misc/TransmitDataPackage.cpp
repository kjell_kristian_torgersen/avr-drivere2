#include <stdarg.h>
#include <avr/pgmspace.h>
#include "config.h"

namespace MISC {

void TransmitDataPackage(bput_ptr bput, const byte * payload, const uint8_t length)
{
	uint16_t checksum = 0;
	bput(PACKAGE_START_BYTE);
	checksum += (uint16_t) PACKAGE_START_BYTE;
	bput(length);
	checksum += (length);
	for (uint8_t i = 0; i < length; i++) {
		bput(payload[i]);
		checksum += payload[i];
	}
	while (checksum > 0x100) {
		checksum = (checksum >> 8) + (checksum & 0xFF);
	}
	bput((byte) (~checksum));
}

}
