#include <stdarg.h>
#include <avr/pgmspace.h>
#include "config.h"
#include "util/misc.hpp"

namespace MISC {

extern void printuint(bput_ptr bput, uint32_t u, uint32_t base);

// Gjør formatert utskrift med egen funksjon for å putte ut karakterer. format-streng må være i program-minne, resten i dataminne.
int16_t xprintf_p(bput_ptr bput, const char * format, ...)
{
	uint16_t i = 0;
	va_list valist;
	va_start(valist, format);
	while (pgm_read_byte(&format[i]) != '\0') {
		// Skal vi sette inn noe?
		if (pgm_read_byte(&format[i]) == '%') {
			i++;
			if (pgm_read_byte(&format[i]) == 'l') // long, hopp over for nå, sjekker senere.
				i++;
			// Hva skal vi sette inn?
			switch (pgm_read_byte(&format[i])) {
			case 'i': // signed integer
			{
				//int8_t buf[11];
				int32_t _int;

				if (pgm_read_byte(&format[i-1]) == 'l') {
					_int = va_arg(valist, int32_t);
				} else {
					_int = va_arg(valist, int16_t);
				}

				if (_int < 0) {
					bput('-');
					_int = -_int;
				}
				printuint(bput, _int, 10);
				//xputs(bput, uint2str(buf, 11, _int));
			}
				break;
			case 'u': // uint16_teger
			{
				//int8_t buf[11];
				uint32_t _uint;
				if (pgm_read_byte(&format[i-1]) == 'l') {
					_uint = va_arg(valist, uint32_t);
				} else {
					_uint = va_arg(valist, uint16_t);
				}
				printuint(bput, _uint, 10);
			}
				break;
			case 'x': // hex
			{
				const int8_t _hex[] = "0123456789abcdef";
				uint32_t _uint;
				if (pgm_read_byte(&format[i-1]) == 'l') {
					_uint = va_arg(valist, uint32_t);
					for (uint16_t j = 32; j != 0; j -= 4) {
						bput(_hex[(_uint >> (j - 4)) & 0xF]);
					}
				} else {
					_uint = va_arg(valist, uint16_t);
					for (uint16_t j = 16; j != 0; j -= 4) {
						bput(_hex[(_uint >> (j - 4)) & 0xF]);
					}
				}

			}
				break;
			case 'X': // HEX
			{
				const int8_t _hex[] = "0123456789ABCDEF";
				uint32_t _uint;
				if (pgm_read_byte(&format[i-1]) == 'l') {
					_uint = va_arg(valist, uint32_t);
					for (uint16_t j = 32; j != 0; j -= 4) {
						bput(_hex[(_uint >> (j - 4)) & 0xF]);
					}
				} else {
					_uint = va_arg(valist, uint16_t);
					for (uint16_t j = 16; j != 0; j -= 4) {
						bput(_hex[(_uint >> (j - 4)) & 0xF]);
					}
				}

			}
				break;
			case 'c': // int8_t
			{
				int8_t c = (int8_t) va_arg(valist, int16_t);
				bput(c);
			}
				break;
			case 's': { // streng
				char *str = va_arg(valist, char*);
				xputs(bput, str);
			}
				break;
			case 'S': { // streng fra flash
				char *str = va_arg(valist, char*);
				xputs_p(bput, str);
			}
				break;
			case '%': { // streng
				bput('%');
			}

				break;
			}
			i++;
		} else { // ellers, skriv ut tegn
			bput(pgm_read_byte(&format[i]));
			i++;
		}
	}
	va_end(valist);
	return 0;
}
}
