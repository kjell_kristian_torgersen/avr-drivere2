#include <stdarg.h>
#include <avr/pgmspace.h>
#include "config.h"

namespace MISC {

static byte * package;
static uint8_t packageState = 0;
static uint16_t checksum;
static uint8_t PACKAGE_MAX_SIZE;

static void (*packageReceivedCallback)(byte * package) = nullptr;

void PackageInit(byte* packagebuffer, uint8_t buffersize, void (*PackageReceivedCallback)(byte * package))
{
	package = packagebuffer;
	PACKAGE_MAX_SIZE = buffersize;
	packageReceivedCallback = PackageReceivedCallback;
}

void ReceivePackage(byte b)
{
	switch (packageState) {
	case 0: // vent på start byte
		if (b == PACKAGE_START_BYTE) {
			packageState++;
			checksum = b;
		}
		break;
	case 1: // lagre størrelse
		package[packageState - 1] = b;
		checksum += b;
		packageState++;
		break;
	default: //lagre resten
		package[packageState - 1] = b;
		checksum += b;
		packageState++;
		if (packageState >= package[0] + 3 || packageState >= PACKAGE_MAX_SIZE) {
			while (checksum > 0xFF) {
				checksum = (checksum >> 8) + (checksum & 0xFF);
			}
			if (checksum == 0xFF) packageReceivedCallback(package);
			packageState = 0;
		}
		break;
	}
}

}
