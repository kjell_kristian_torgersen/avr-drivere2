#include <stdarg.h>
#include <avr/pgmspace.h>
#include "config.h"

namespace MISC {

void printuint(bput_ptr bput, uint32_t u, uint32_t base)
{
	const char * hex = "0123456789ABCDEF";
	uint32_t a;
	if (base > 1) {
		if ((a = u / base)) { // Skal være slik = ikke ==
			printuint(bput, a, base);
		}
		bput((hex[(u % base)]));
	}
}
}
