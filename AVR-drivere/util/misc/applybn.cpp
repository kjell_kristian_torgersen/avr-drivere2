#include <stdarg.h>
#include <avr/pgmspace.h>
#include "config.h"

namespace MISC {

// Skriv ut en tekststring fra RAM
void applybn(bput_ptr bput, const char * str, const uint16_t n)
{

	for (uint16_t i = 0; i < n; i++) {
		bput(str[i]);
	}

}
}
