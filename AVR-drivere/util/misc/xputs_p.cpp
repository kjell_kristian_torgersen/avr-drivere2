#include <stdarg.h>
#include <avr/pgmspace.h>
#include "config.h"

namespace MISC {

// Skriv ut en tekststring fra programminne
void xputs_p(bput_ptr bput, const char * str)
{
	uint16_t i = 0;
	int16_t c;
	while ((c = pgm_read_byte(&str[i])) != '\0') {
		bput(c);
		i++;
	}
}
}
