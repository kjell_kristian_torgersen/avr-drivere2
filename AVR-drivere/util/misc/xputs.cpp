#include <stdarg.h>
#include <avr/pgmspace.h>
#include "config.h"

namespace MISC {

// Skriv ut en tekststring fra RAM
void xputs(bput_ptr bput, const char * str)
{
	int16_t i = 0;
	while (str[i] != '\0') {
		bput(str[i]);
		i++;
	}
}
}
