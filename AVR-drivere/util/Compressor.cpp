#include "config.h"
#include "util/Compressor.hpp"

namespace CMP {

void (*Compressor::ByteWrite)(byte byte) = 0;
byte Compressor::_byte = 0;
bool Compressor::high = true;

void Compressor::WriteNibble(byte nibble)
{
	if (high) {
		_byte = (nibble << 4) & 0xF0;
		high = 0;
	} else {
		_byte |= (nibble) & 0xF;
		high = 1;
		if (ByteWrite)
			ByteWrite(_byte);
	}
}

void Compressor::Repeat()
{
	WriteNibble(0xC | (counter >> 4)); // write 11XX 2 msb of counter
	WriteNibble((counter & 0xF)); // write XXXX 4 lsb of counter
	counter = 0;
}

void Compressor::WriteValue(uint16_t current)
{
	int16_t diff = current - previous;
	if (diff < -4 || diff > 3) { // outside limits, write whole value, 6 bit
		WriteNibble((0x8 | ((current >> 6))) & 0xB); // write 10XX 2 MSB of current value
		//WriteNibble((0x8 | ((current >> 4)))); // write  MSB
		WriteNibble((current >> 2) & 0xF); // XXXX write 4 LSB of current value
		previous = (current >> 2) << 2;
	} else { // within limits, write 0XXX 3 bit for difference
		WriteNibble(diff & 0x7);
		previous = current;
	}
}

void Compressor::Write(uint16_t current)
{
	if (previous != current) {
		if (counter == 0) {
			WriteValue(current); // ikke repetisjon av forrige verdi
			//*previous = current;
		} else {
			// 1 eller flere repetisjoner av samme verdi men n� en annen verdi
			Repeat(); // Skriv antall repetisjoner av forrige
			WriteValue(current); // skriv n�v�rende
		}
	} else { // samme verdi som sist inkrementer teller
		counter++;
		if (counter > 61) {
			Repeat(); // Skriv antall repetisjoner av forrige
		}
	}
}

Compressor::Compressor() :
		previous(0), counter(0)
{

}
} /* namespace CMP */

