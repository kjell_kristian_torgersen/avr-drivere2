#include "config.h"
#include "util/RingBuffer.hpp"
#include "util/misc.hpp"

namespace MISC {

RingBuffer::RingBuffer(byte * buffer, uint8_t size) : read(0), write(0), size(size), buffer(buffer)
{
}

int16_t RingBuffer::Read(void)
{
	int16_t ret = -1;
	if (write != read) {
		ret = buffer[read];
		read++;
		if (read == size)
			read = 0;
	}
	return ret;
}

void RingBuffer::Write(byte c)
{
	buffer[write] = c;
	write++;
	if (write == size)
		write = 0;
}
}

uint8_t MISC::RingBuffer::Count() {
	return (write - read) & (size - 1);
}

uint8_t MISC::RingBuffer::Capacity()
{
	return size;
}
