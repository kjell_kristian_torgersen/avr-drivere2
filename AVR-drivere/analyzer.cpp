#include "headers/HAL/MCU.hpp"
#include "headers/HAL/K6BEZ_DDS.hpp"
#include "headers/util/misc.hpp"

#include <avr/sleep.h>

struct Measurement {
	uint16_t fwd;
	uint16_t rev;
};

enum class Mode {
	Idle, BinarySweep, K6BEZ_Sweep
};

struct Settings {
	uint32_t fstart;
	uint32_t fdiff;
	uint32_t fsteps;
};

byte _rxbuf[8];
MISC::RingBuffer uartrxbuf(_rxbuf, sizeof(_rxbuf));

byte _txbuf[128];
MISC::RingBuffer uarttxbuf(_txbuf, sizeof(_txbuf));

Settings settings = { 1000000, 290000, 100 };

uint8_t state = 0;

volatile uint16_t conversions = 0;

uint32_t number = 0;
uint32_t fstart = 1000000;
uint32_t fstop = 30000000;
uint32_t fsteps = 100;
uint32_t currentFreq = 0;
uint32_t currentStep = 0;
volatile uint32_t valueSum = 0;

Mode mode = Mode::Idle;

static inline void ADC_newValue(uint16_t newSample)
{
	if (conversions) {
		valueSum += newSample;
		conversions--;
		if (conversions == 0) {
			MCU::ADC_Stop();
				//sleep_disable();
		}
	}
}

static inline uint16_t readChannel(char channel)
{
	valueSum = 0;
	conversions = 32;
	set_sleep_mode(SLEEP_MODE_IDLE);
	sleep_enable();
	sleep_bod_disable();
	MCU::ADC_Init(ADC_newValue, 1, channel);
	while (conversions){
		sleep_cpu();
	}
	
	return (valueSum);
}

static Measurement doSweep(uint32_t freq)
{
	Measurement measurement;
	//MISC::xputs_p(MCU::UART_putchar, PSTR("Doing sweep.\r\n"));
	K6BEZ_DDS::SetFrequency(freq);
	//MCU::Delay_us(1000);
	//MISC::xputs_p(MCU::UART_putchar, PSTR("DDS setup.\r\n"));
	measurement.fwd = readChannel(1);
	//MISC::xputs_p(MCU::UART_putchar, PSTR("FWD done.\r\n"));
	measurement.rev = readChannel(0);
	//MISC::xputs_p(MCU::UART_putchar, PSTR("REV done.\r\n"));
	return measurement;
}

static inline void handleCommunication(char c)
{
	if (state == 0) {
		switch (c) {
		case 'U':
			state = 1;
			break;
		case 'X':
			mode = Mode::BinarySweep;
			currentStep = 0;
			currentFreq = settings.fstart;
			break;
		case 'A':
			fstart = number;
			number = 0;
			break;
		case 'B':
			fstop = number;
			number = 0;
			break;
		case 'C':
			doSweep(number*4294967295/125000000);
			number = 0;
			break;
		case 'N':
			fsteps = number;
			number = 0;
			break;
		case '?':
			MISC::xprintf_p(MCU::UART_putchar, PSTR("Start Freq: %lu\r\n"), settings.fstart);
			MISC::xprintf_p(MCU::UART_putchar, PSTR("Stop Freq: %lu\r\n"), settings.fstart + settings.fsteps*settings.fdiff);
			MISC::xprintf_p(MCU::UART_putchar, PSTR("Num Steps: %lu\r\n"), settings.fsteps);
			break;
		case 'S':
		case 's':
			settings.fstart = fstart;
			settings.fdiff = (fstop - fstart) / fsteps;
			settings.fsteps = fsteps;
			currentFreq = fstart;
			currentStep = 0;
			mode = Mode::K6BEZ_Sweep;
			break;
		//case '\n':
//			MISC::xprintf_p(MCU::UART_putchar, PSTR("number: %lu\r\n"), number);
	//		number = 0;
		default:
			if (c >= '0' && c <= '9') {
				number = 10 * number + (c - '0');
			}
			break;
		}
	} else {
		((char*) &settings)[state - 1] = c;
		state++;
		if ((state - 1) == sizeof(struct Settings)) {
			state = 0;
			//MISC::xputs_p(MCU::UART_putchar, PSTR("Starting sweep.\r\n"));
		}
	}
}



static inline void BinarySweepStep()
{
	Measurement measurement = doSweep(currentFreq);
	MISC::applybn(MCU::UART_putchar, (char*) (&measurement), sizeof(Measurement));
	//MISC::xputs_p(MCU::UART_putchar, PSTR("UART TX done.\r\n"));
	currentFreq += settings.fdiff;
	currentStep++;
	if (currentStep >= settings.fsteps) {
		mode = Mode::Idle;
		//MISC::xputs_p(MCU::UART_putchar, PSTR("Sweep done.\r\n"));
	}
}

static inline void K6BEZ_SweepStep()
{
	Measurement measurement = doSweep(currentFreq*4294967295/125000000);
	float fwd = measurement.fwd;
	float rev = measurement.rev;
	float swr = (fwd + rev) / (fwd - rev);
	MISC::xprintf_p(MCU::UART_putchar, PSTR("%lu,%lu,%u,%u\r\n"), currentFreq, (uint32_t) (1000.0 * swr), measurement.fwd, measurement.rev);
	currentFreq += settings.fdiff;
	currentStep++;
	if (currentStep >= settings.fsteps) {
		MISC::xputs_p(MCU::UART_putchar, PSTR("End\r\n"));
		mode = Mode::Idle;
	}
}

int main()
{
	DIDR0 |= 0x3;
	DDRD |= (1 << 3) | (1 << 4) | (1 << 5);
	//MCU::DisableInterrupts();
	MCU::UART_Init(UART_PRESCALER(115200), &uarttxbuf, &uartrxbuf);
	MCU::EnableInterrupts();
	//MISC::xputs_p(MCU::UART_putchar, PSTR("Program started.\r\n"));
	for (;;) {
		switch (mode) {
		case Mode::BinarySweep:
			//MISC::xputs_p(MCU::UART_putchar, PSTR("Doing sweep.\r\n"));
			BinarySweepStep();
			break;
		case Mode::K6BEZ_Sweep:
			K6BEZ_SweepStep();
			break;
		case Mode::Idle:
			
			break;
		}

		int c = uartrxbuf.Read();
		if (c != -1) {
			handleCommunication(c);
		}
	}
}
