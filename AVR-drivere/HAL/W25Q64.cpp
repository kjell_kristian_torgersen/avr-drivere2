#include "config.h"
#include "util/misc.hpp"
#include "HAL/MCU.hpp"


namespace W25Q64 {

enum class W25Q64Cmd
{
	WriteEnable = 0x6, WriteDisable = 0x4, ReadStatus=0x05, PageProgram = 0x2, ChipErase = 0x60, PowerDown = 0xB9, PowerUp = 0xAB, ReadData = 0x3
};

void ChipSelect(bool val)
{
	if (val) {
		//PORTB &= ~1;
		MCU::BitClear(1, 1);
		//_delay_us(10);
	} else {
		MCU::BitSet(1, 1);
		//_delay_us(10);
	}
}
void ExecuteCommand(W25Q64Cmd cmd)
{
	ChipSelect(true);
	MCU::SPI_putchar((byte) cmd);
	ChipSelect(false);
}
/** \brief Write to the W25Q64 FLASH chip. \warning offset+n must not exceed the page size of 256 bytes. Then it wraps around and rewrites from start.
 * \param page Select which page to write to
 * \param offset Offset into that page
 * \param *data Data to write
 * \param n How many to write from *data to FLASH.*/
void Write(const uint16_t page, const uint8_t offset, const byte * data, const uint16_t n)
{
	byte cmd[] = { (byte) W25Q64Cmd::PageProgram, (byte) (page >> 8), (byte) (page & 0xFF), offset };

	ExecuteCommand(W25Q64Cmd::WriteEnable);

	ChipSelect(true);
	MISC::applybn(MCU::SPI_putchar, (const char*) cmd, sizeof(cmd));
	MISC::applybn(MCU::SPI_putchar, (const char*) data, n);
	ChipSelect(false);

	ExecuteCommand(W25Q64Cmd::WriteDisable);
}

void ChipErase(void)
{
	ExecuteCommand(W25Q64Cmd::WriteEnable);
	ExecuteCommand(W25Q64Cmd::ChipErase);
	ExecuteCommand(W25Q64Cmd::WriteDisable);
}

void Read(const uint16_t page, const uint8_t offset, byte * data, const uint16_t n)
{
	byte cmd[] = { (byte) W25Q64Cmd::ReadData, (byte) (page >> 8), (byte) (page & 0xFF), offset };

	ChipSelect(true);
	MISC::applybn(MCU::SPI_putchar, (const char*) cmd, sizeof(cmd));
	for (uint16_t i = 0; i < n; i++) {
		data[i] = MCU::SPI_putchar(0);
	}
	ChipSelect(false);
}

byte Status(void)
{
	byte ret;
	ChipSelect(true);
	MCU::SPI_putchar((int8_t)W25Q64Cmd::ReadStatus);
	ret = MCU::SPI_putchar(0);
	ChipSelect(false);
	return ret;
}
}
