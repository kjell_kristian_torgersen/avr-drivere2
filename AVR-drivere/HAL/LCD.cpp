#include "config.h"
#include "HAL/MCU.hpp"
#include "HAL/PCF8574.hpp"
#include "HAL/LCD.hpp"
#include "LCD_priv.hpp"

namespace LCD {

static byte _output; // For å holde styr på hva vi har ut på LCD. (Dette fordi vi ofte bare endrer en eller noen få pinner om gangen)
static uint8_t _x; // x posisjon for å holde styr på når vi skal skrolle
static byte _scrollbuffer[16]; // buffer for 1 linje
static byte _address;

bool enabled = false;

// Toggle the en pin
void Toggle(void)
{
	PCF8574::SetOutput(PCF8574_ADDRESS, _output);
	_output |= (1 << E_PIN);
	PCF8574::SetOutput(_address, _output);
	_output &= ~(1 << E_PIN);
	PCF8574::SetOutput(_address, _output);
}

/** \brief Skru av og på LED
 * \param onoff 0 for av, ellers på. */
void LED(uint8_t onoff)
{
	if (onoff) {
		_output |= _BV(LED_PIN);
	} else {
		_output &= ~_BV(LED_PIN);

	}
	PCF8574::SetOutput(_address, _output);

}

/** \brief Skriv en kommando eller karakter til displayet
 * \param data Karakter eller kommando
 * \param rs 0 for kommando, ellers data
 */

void Write(byte data, uint8_t rs)
{
	if (rs) {
		_output |= (1 << RS_PIN);
	} else {
		_output &= ~(1 << RS_PIN);
	}
	PCF8574::SetOutput(_address, _output);
	_output &= ~DATA_PINS;
	_output |= (data & 0x80) ? (1 << DATA3_PIN) : 0;
	_output |= (data & 0x40) ? (1 << DATA2_PIN) : 0;
	_output |= (data & 0x20) ? (1 << DATA1_PIN) : 0;
	_output |= (data & 0x10) ? (1 << DATA0_PIN) : 0;
	Toggle();
	_output &= ~DATA_PINS;
	_output |= (data & 0x08) ? (1 << DATA3_PIN) : 0;
	_output |= (data & 0x04) ? (1 << DATA2_PIN) : 0;
	_output |= (data & 0x02) ? (1 << DATA1_PIN) : 0;
	_output |= (data & 0x01) ? (1 << DATA0_PIN) : 0;
	Toggle();
	_output |= DATA_PINS;
	PCF8574::SetOutput(_address, _output);
}

/** \brief Send en kommando til diplayet. */
void Command(byte cmd)
{
	Write(cmd, 0);
}

/** \brief Send en karakter til diplayet. */
void Data(byte data)
{
	Write(data, 1);
}

/** \brief Initialiser displayet.
 * \param dispAttr Se LCD_DISP_* i LCD.hpp for mer informasjon */
uint8_t Init(byte dispAttr, byte address)
{
	enabled = false;
	_address = address;

	PCF8574::Init(0x7F);

	_output = 0;
	if(PCF8574::SetOutput(_address, _output) == 1) return 1;
	MCU::Delay_ms(16);
	_output = (1 << DATA0_PIN) | (1 << DATA1_PIN);
	Toggle();
	MCU::Delay_us(4992);
	Toggle();
	MCU::Delay_us(64);
	Toggle();
	MCU::Delay_us(64);
	_output = (1 << DATA1_PIN);
	Toggle();
	MCU::Delay_us(64);
	Command(LCD_FUNCTION_4BIT_2LINES);
	Command(LCD_DISP_OFF);
	Clear();
	Command(LCD_MODE_DEFAULT);
	Command(dispAttr);
	LED(1);
	GotoXY(0, 1);
	enabled = true;
	return 0;
}

/** \brief Brukerfunksjon for å skrive ut en karakter til LCD.
 * \param c Karakter å skrive ut
 * \return Samme karakter som skrevet ut*/
byte putchar(byte c)
{
	if(!enabled) return c;
	if (c == '\n' || c == '\r') {
		Write(' ', 1);
		_scrollbuffer[_x] = ' ';
	} else if (c == '\b') {
		if (_x) {
			_x--;
			GotoXY(_x, 1);
			Write(' ', 1);
			GotoXY(_x, 1);
			return c;
		}
	} else {
		Write(c, 1);
		_scrollbuffer[_x] = c;
	}

	_x++;

	if (_x >= 16 || c == '\n' || c == '\r') {

		//_y = (_y + 1) & 1;
		// Kopier fra linje 2 til linje 1 (for skrollefunksjon)
		GotoXY(0, 0);
		for (int i = 0; i < 16; i++) {
			if (i < _x) {
				Write(_scrollbuffer[i], 1);
			} else {
				Write(' ', 1);
			}
		}

		// Rensk linje 2
		GotoXY(0, 1);
		for (int i = 0; i < 16; i++) {
			Write(' ', 1);
		}
		// Gå til begynnelsen av linje 2
		GotoXY(0, 1);
		_x = 0;
	}

	return c;
}

/** \brief Gå til angitt lokasjon på displayet
 * \param x 0 til 15
 * \param y 0 eller 1*/
void GotoXY(uint8_t x, uint8_t y)
{
	if(!enabled) return;
	if (y == 0) Command((1 << LCD_DDRAM) + LCD_START_LINE1 + x);
	else Command((1 << LCD_DDRAM) + LCD_START_LINE2 + x);
}

/** \brief Rensk displayet. */
void Clear(void)
{
	if(!enabled) return;
	Command(1 << LCD_CLR);
}
}
