#include "config.h"

#include "HAL/PCF8574.hpp"
#include "HAL/MCU.hpp"

namespace PCF8574 {
void Init(uint8_t prescaler)
{
	MCU::I2C_Init(prescaler);
}

uint8_t SetOutput(byte address, byte data)
{
	byte ret = MCU::I2C_Start((address << 1) | I2C_WRITE);
	if(ret == 1) return 1;
	MCU::I2C_Write(data);
	MCU::I2C_Stop();
	return 0;
}

}
