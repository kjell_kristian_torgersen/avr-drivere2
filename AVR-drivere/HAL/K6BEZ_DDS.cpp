#include <avr/io.h>
namespace K6BEZ_DDS {
static inline void send_byte(uint8_t data_to_send)
{
	// Bit bang the byte over the SPI bus
	for (int i = 0; i < 8; i++, data_to_send >>= 1) {
		// Set Data bit on output pin
		if (data_to_send & 0x01) {
			PORTD |= 1 << 3;
		} else {
			PORTD &= ~(1 << 3);
		}

		// Strobe the clock pin
		PORTD |= 1 << 5;
		PORTD &= ~(1 << 5);
	}
}

void SetFrequency(uint32_t f)
{
	// Calculate the DDS word - from AD9850 Datasheet
	//int32_t f = Freq_Hz * 4294967295 / 125000000;

	// Send one byte at a time
	for (int b = 0; b < 4; b++, f >>= 8) {
		send_byte(f & 0xFF);
	}

	// 5th byte needs to be zeros
	send_byte(0);

	// Strobe the Update pin to tell DDS to use values
	PORTD |= 1 << 4;
	PORTD &= ~(1 << 4);
}
}
