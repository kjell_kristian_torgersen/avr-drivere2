#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "config.h"
#include "util/RingBuffer.hpp"
#include "HAL/MCU.hpp"

namespace MCU {

MISC::RingBuffer * _uarttxbuf;
MISC::RingBuffer * _uartrxbuf;

//static void (*uartrx_callback)(char byte) = nullptr;

/** prescaler = (((2 * F_CPU / (baudrate * 16UL))) - 1)*/
//, void (*uartrx_cb)(char byte),
void UART_Init(uint16_t prescaler, MISC::RingBuffer * uarttxbuf, MISC::RingBuffer * uartrxbuf)
{
	DDRD |= 1 << 1;
	//uint16_t prescale = (((2 * F_CPU / (baudrate * 16UL))) - 1);
	UCSR0B = ((1 << RXEN0) | (1 << TXEN0)); // Turn on the transmission and reception circuitry
	UCSR0C = ((0 << UMSEL00) | (1 << UCSZ00) | (1 << UCSZ01)); // Use 8-bit int8_tacter sizes
	UCSR0A = 1 << U2X0;
	UBRR0H = (prescaler >> 8); // Load upper 8-bits of the baud rate value into the high byte of the UBRR register
	UBRR0L = prescaler; // Load lower 8-bits of the baud rate value into the low byte of the UBRR register
	if (uarttxbuf) {
		UCSR0B |= (1 << RXCIE0) | (1 << TXCIE0); // Enable the USART Recieve Complete interrupt (USART_RXC)
	} else {
		UCSR0B |= (1 << RXCIE0) | (0 << TXCIE0); // Enable the USART Recieve Complete interrupt (USART_RXC)
	}
	//uartrx_callback = uartrx_cb;
	_uarttxbuf = uarttxbuf;
	_uartrxbuf = uartrxbuf;
}

byte UART_putchar(byte c)
{
	if (_uarttxbuf == nullptr) { // no transmit buffer have been provided, transmit without buffering and interrupts
		while (!(UCSR0A & (1 << UDRE0)))
			;
		UDR0 = c;
	} else { // buffer available, use buffer and interrupt to transmit
		bool u;
		do { // if buffer is full, loop until there is room for one byte
			MCU::DisableInterrupts();
			u = _uarttxbuf->write == ((_uarttxbuf->read - 1) & (_uarttxbuf->size-1));
			MCU::EnableInterrupts();
		} while (u);
		// add new character to buffer
		MCU::DisableInterrupts();
		_uarttxbuf->Write(c);
		// start transmit interrupt if not already running
		if (UCSR0A & (1 << UDRE0)) { 
			UDR0 = _uarttxbuf->Read();
		}
		MCU::EnableInterrupts();
	}

	return c;
}

/*int16_t UART_getchar(void)
 {
 int16_t ret = -1;
 if (uart_buf_read != uart_buf_write) {
 ret = uart_buf_data[(uint16_t) uart_buf_read];
 uart_buf_read = (uart_buf_read + 1) & BUF_MASK;
 }
 return ret;
 }*/

ISR(USART_RX_vect)
{
	_uartrxbuf->Write((byte) UDR0);
}

ISR(USART_TX_vect)
{
	int c = _uarttxbuf->Read();
	if (c != -1) {
		UDR0 = c;
	} 
}

}
