#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "config.h"

static volatile uint16_t _TIMER_ds = 0;
static volatile uint16_t _TIMER_s = 0;

static void (*TIMER_tick)(void) = 0;
static void (*TIMER_sec)(void) = 0;

// Initialiser for timerinterrupt, count = 12499 for 100 ms
void TIMER_Init(unsigned int count)
{
	TCCR1B |= (1 << WGM12);
	OCR1A = count; // 100 ms
	TIMSK1 |= (1 << OCIE1A);
	TCCR1B |= (0 << CS12) | (1 << CS11) | (0 << CS10); // 1MHz/8

}

/** Interruptsikker utlesning av ms ticks */
uint16_t TIMER_ticks(void)
{
	cli();
	uint16_t ret = _TIMER_ds;
	sei();
	return ret;
}

/** Interruptsikker utlesning av sekund ticks */
uint16_t TIMER_s(void)
{
	cli();
	uint16_t ret = _TIMER_s;
	sei();
	return ret;
}

ISR(TIMER1_COMPA_vect)
{
	static uint16_t TIMER_ds2 = 0;
	//sleep_disable();          // Disable Sleep on Wakeup
	_TIMER_ds++; //Et tick har passert.
	TIMER_ds2++;
	if (TIMER_tick != 0)
		TIMER_tick();
	if (TIMER_ds2 >= 160) {
		TIMER_ds2 = 0;
		_TIMER_s++;
		if (TIMER_sec != 0)
			TIMER_sec();
	}
}
