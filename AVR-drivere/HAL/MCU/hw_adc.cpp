#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "config.h"

namespace MCU {

static void (*_adc_callback)(uint16_t newSample) = nullptr;


void ADC_Init(void (*adc_callback)(uint16_t newSample), char ref, char channel)
{
	_adc_callback = adc_callback;
	//ADMUX = (ref << REFS0) | (0 << ADLAR) | (0 << MUX3) | (0 << MUX2) | (0 << MUX1) | (0 << MUX0);
	//DIDR0 |= 1 << channel;
	ADMUX = (ref << REFS0) | (0 << ADLAR) | channel;
	ADCSRA = (1 << ADEN) | (1 << ADSC) | (1 << ADATE) | (0 << ADIF) | (1 << ADIE) | (1 << ADPS2) | (0 << ADPS1) | (1 << ADPS0);
	ADCSRB = (0 << ACME) | (0 << ADTS2) | (0 << ADTS1) | (0 << ADTS0);


}

void ADC_Stop() {
	ADCSRA = 0;
}

ISR(ADC_vect)
{
	_adc_callback(ADC);
}

}
