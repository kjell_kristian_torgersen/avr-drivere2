#include <avr/io.h>
#include <avr/interrupt.h>

#include <util/delay.h>
#include "config.h"

namespace MCU {

void Delay_us(uint16_t us) {
#if F_CPU==16000000
	_delay_loop_2(us << 2);
#elif F_CPU==8000000
	_delay_loop_2(us << 1);
#elif F_CPU==4000000
	_delay_loop_2(us);
#elif F_CPU==2000000
	_delay_loop_2(us >> 1);
#elif F_CPU==1000000
	_delay_loop_2(us >> 2);
#else
#error Make an elif preprocessor directive here
#endif

}

void Delay_ms(uint16_t ms) {
	for (uint16_t i = 0; i < ms; i++) {
		__builtin_avr_delay_cycles((const uint32_t) F_CPU / 1000UL);
	}
}

void EnableInterrupts(void) {
	sei();
}

void DisableInterrupts(void) {
	cli();
}

void BitSet(uint8_t port, uint8_t mask) {
	switch (port) {
	case 0:
		//PORTA |= mask;
		break;
	case 1:
		PORTB |= mask;
		break;
	case 2:
		PORTC |= mask;
		break;
	case 3:
		PORTD |= mask;
		break;
	case 4:
		//PORTA |= mask;
		break;
	case 5:
		DDRB |= mask;
		break;
	case 6:
		DDRC |= mask;
		break;
	case 7:
		DDRD |= mask;
		break;
	}
}
void BitClear(uint8_t port, uint8_t mask) {
	switch (port) {
	case 0:
		//PORTA |= mask;
		break;
	case 1:
		PORTB &= ~mask;
		break;
	case 2:
		PORTC &= ~mask;
		break;
	case 3:
		PORTD &= ~mask;
		break;
	case 4:
		//PORTA |= mask;
		break;
	case 5:
		DDRB &= ~mask;
		break;
	case 6:
		DDRC &= ~mask;
		break;
	case 7:
		DDRD &= ~mask;
		break;
	}
}
}
