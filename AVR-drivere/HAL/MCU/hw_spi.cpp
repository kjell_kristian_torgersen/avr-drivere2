#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "config.h"

namespace MCU {

#define DDR_SPI DDRB
#define DD_SS (2)
#define DD_MOSI (3)
#define DD_MISO (4)
#define DD_SCK (5)

void SPI_Init(void)
{
	/* Set MOSI and SCK output. MISO as input */
	PORTB |= 1 << DD_SS;
	DDR_SPI |= (1 << DD_MOSI) | (1 << DD_SCK) | (1 << DD_SS);
	DDR_SPI &= ~(1 << DD_MISO);

	/* Enable SPI, Master, set clock rate fck/16 */
	SPCR = (0 << SPIE) | (1 << SPE) | (0 << DORD) | (1 << MSTR) | (0 < CPOL) | (0 << CPHA) | (1 << SPR1) | (1 << SPR0);
}

byte SPI_putchar(byte c)
{
	//MCU::UART_putint8_t('-');
	// Load data into the buffer
	SPDR = c;

	//Wait until transmission complete
	while (!(SPSR & (1 << SPIF)))
		;

	// Return received data
	//MCU::UART_putint8_t('+');
	return SPDR;
}
}
