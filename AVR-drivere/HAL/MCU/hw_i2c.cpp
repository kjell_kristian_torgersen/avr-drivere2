#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <util/twi.h>
#include "config.h"

namespace MCU {

//byte UART_putchar(byte c);

void I2C_Init(uint8_t prescaler)
{
	TWSR = 0; /* no prescaler */
	TWBR = prescaler; /* must be > 10 for stable operation */
}

/** \brief Issues a start condition and sends address and transfer direction.
  return 0 = device accessible, 1= failed to access device */
int I2C_Start(unsigned char address)
{
	uint32_t counter = 0;
	byte twst;

	// send START condition
	TWCR = (1 << TWINT) | (1 << TWSTA) | (1 << TWEN);

	// wait until transmission completed
	//UART_putchar('1');
	while (!(TWCR & (1 << TWINT))) {
		counter++;
		if (counter == 0) return 1;
	}

	// check value of TWI Status Register. Mask prescaler bits.
	twst = TW_STATUS & 0xF8;
	if ((twst != TW_START) && (twst != TW_REP_START)) return 1;

	// send device address
	TWDR = address;
	TWCR = (1 << TWINT) | (1 << TWEN);

	// wail until transmission completed and ACK/NACK has been received
	//UART_putchar('2');
	counter = 0;
	while (!(TWCR & (1 << TWINT))) {
		counter++;
		if (counter == 0) return 1;
	}

	//UART_putchar('3');
	// check value of TWI Status Register. Mask prescaler bits.
	twst = TW_STATUS & 0xF8;
	if ((twst != TW_MT_SLA_ACK) && (twst != TW_MR_SLA_ACK)) return 1;
	return 0;
}

void I2C_Stop(void)
{
	uint32_t counter = 0;
	/* send stop condition */
	TWCR = (1 << TWINT) | (1 << TWEN) | (1 << TWSTO);
	// wait until stop condition is executed and bus released
	while (TWCR & (1 << TWSTO)) {
		counter++;
		if (counter == 0) break;
	}

}/* i2c_stop */

byte I2C_Read(byte ack)
{
	uint32_t counter = 0;
	if (ack) {
		TWCR = (1 << TWINT) | (1 << TWEN) | (1 << TWEA);
	} else {
		TWCR = (1 << TWINT) | (1 << TWEN);
	}
	//UART_putchar('4');
	while (!(TWCR & (1 << TWINT))) {
		counter++;
		if (counter == 0) break;
	}

	return TWDR;
}
byte I2C_Write(byte data)
{
	uint32_t counter = 0;
	byte twst;

	// send data to the previously addressed device
	TWDR = data;
	TWCR = (1 << TWINT) | (1 << TWEN);
	//UART_putchar('5');
	// wait until transmission completed
	while (!(TWCR & (1 << TWINT))) {
		counter++;
		if (counter == 0) return 1;
	}

	// check value of TWI Status Register. Mask prescaler bits
	twst = TW_STATUS & 0xF8;
	if (twst != TW_MT_DATA_ACK) return 1;
	return 0;
}
}
