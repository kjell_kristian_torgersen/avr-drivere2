#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "config.h"

namespace MCU {
//#define SPEED 416
int16_t SWUART_putchar(uint16_t SPEED, volatile uint8_t & PORT, uint8_t pin, uint8_t byte)
{
	cli();
	PORT &= ~pin; // start bit
	_delay_loop_2(SPEED);

	// data bits
	for (int8_t i = 0; i < 8; i++) {
		if (byte & (1 << (i))) {
			PORT |= pin;
		} else {
			PORT &= ~pin;
		}
		_delay_loop_2(SPEED);
	}
	// stop bit
	PORT |= pin;
	_delay_loop_2(SPEED);
	sei();
	return byte;
}
}
